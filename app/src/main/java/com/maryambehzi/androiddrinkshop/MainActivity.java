package com.maryambehzi.androiddrinkshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.maryambehzi.androiddrinkshop.Model.CheckUserPassResposce;
import com.maryambehzi.androiddrinkshop.Model.User;
import com.maryambehzi.androiddrinkshop.Model.UserPass;
import com.maryambehzi.androiddrinkshop.Retrofit.IDrinkShopAPI;
import com.maryambehzi.androiddrinkshop.Utils.Common;
import com.maryambehzi.androiddrinkshop.Utils.md5;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;


import java.math.BigInteger;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//TODO accounting and cart items for each user should differ

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISION = 1001;
    TextView txt_Register;
    IDrinkShopAPI mService;
    Button btn_continue;
    String phone;
    FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case  REQUEST_PERMISION:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
            break;
            default:
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, REQUEST_PERMISION);


        mService = Common.getAPI();

        txt_Register = findViewById(R.id.txt_register);
        txt_Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegisterDialog();
            }
        });
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoginPage();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();

    }

    private void startLoginPage(){

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if(firebaseUser != null){
                    Toast.makeText(MainActivity.this, "you are logged in", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                    finish();
                }
                else
                {
                    Toast.makeText(MainActivity.this, "you are not logged in", Toast.LENGTH_SHORT).show();

                }
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Login");

        LayoutInflater inflater = this.getLayoutInflater();
        View login_layout = inflater.inflate(R.layout.login_layout, null);

        final MaterialEditText edt_email = login_layout.findViewById(R.id.edt_email);
        final MaterialEditText edt_password = login_layout.findViewById(R.id.edt_password);
        TextView txt_register = login_layout.findViewById(R.id.txt_register);

        Button btn_login = login_layout.findViewById(R.id.btn_signin);

        builder.setView(login_layout);
        final AlertDialog dialog = builder.create();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                if(TextUtils.isEmpty(edt_email.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter your Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edt_password.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter your Password", Toast.LENGTH_SHORT).show();
                    return;
                }

                byte[] md5Input = edt_password.getText().toString().getBytes();
                BigInteger md5Data = null;

                try {
                    md5Data = new BigInteger(1, md5.encryptMD5(md5Input));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String md5Str = md5Data.toString();

                final android.app.AlertDialog waitingDialog= new
                        SpotsDialog.Builder().setContext(MainActivity.this).build();
                waitingDialog.setMessage("Please wait.....");
                waitingDialog.show();

                mService.checkUserPassExists(edt_email.getText().toString(), md5Str)
                        .enqueue(new Callback<CheckUserPassResposce>() {

                            @Override
                            public void onResponse(Call<CheckUserPassResposce> call, Response<CheckUserPassResposce> response) {
                                waitingDialog.dismiss();
                                CheckUserPassResposce checkUserpassResponse = response.body();
                                if(checkUserpassResponse.isExists()) {

                                    //fetchdata
                                    mService.getUserInformation(edt_email.getText().toString())
                                            .enqueue(new Callback<User>() {
                                                @Override
                                                public void onResponse(Call<User> call, Response<User> response) {
                                                    Toast.makeText(MainActivity.this, "User Logged in Successfully", Toast.LENGTH_SHORT).show();
                                                    waitingDialog.dismiss();
                                                    Common.currentuser = response.body();
                                                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                                    finish();
                                                }

                                                @Override
                                                public void onFailure(Call<User> call, Throwable t) {
                                                    Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                }
                                else{
                                    Toast.makeText(MainActivity.this, checkUserpassResponse.getError_msg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure( Call<CheckUserPassResposce> call, Throwable t) {
                                Toast.makeText(MainActivity.this, "User Logged in Unsuccessfully"+t.getMessage(), Toast.LENGTH_SHORT).show();

                                waitingDialog.dismiss();
                            }
                        });
            }
        });
        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showRegisterDialog();
            }
        });

        dialog.show();
    }

    private void showRegisterDialog(){

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Register");

        LayoutInflater inflater = this.getLayoutInflater();
        View register_layout = inflater.inflate(R.layout.register_layout, null);

        final MaterialEditText edt_email = register_layout.findViewById(R.id.edt_email);
        final MaterialEditText edt_name = register_layout.findViewById(R.id.edt_name);
        final MaterialEditText edt_address = register_layout.findViewById(R.id.edt_address);
        final MaterialEditText edt_birthdate = register_layout.findViewById(R.id.edt_birthdate);
        TextView txt_login = register_layout.findViewById(R.id.txt_login);

        Button btn_register = register_layout.findViewById(R.id.btn_register);

        edt_birthdate.addTextChangedListener(new PatternedTextWatcher("####-##-##"));

        builder.setView(register_layout);
        final AlertDialog dialog = builder.create();

        //Event
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                if(TextUtils.isEmpty(edt_email.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter your Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edt_name.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter your Name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edt_address.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter your Address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edt_birthdate.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter your Birthdate", Toast.LENGTH_SHORT).show();
                    return;
                }

                final android.app.AlertDialog waitingDialog= new
                        SpotsDialog.Builder().setContext(MainActivity.this).build();
                waitingDialog.setMessage("Please wait.....");
                waitingDialog.show();

                mService.registerNewUser(edt_email.getText().toString(),
                        edt_name.getText().toString(),
                        edt_birthdate.getText().toString(),
                        edt_address.getText().toString())
                        .enqueue(new Callback<User>() {

                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                waitingDialog.dismiss();
                                User user = response.body();
                                if(TextUtils.isEmpty(user.getError_msg())) {
                                    phone = edt_email.getText().toString();
                                    Toast.makeText(MainActivity.this, "User Registered Successfully", Toast.LENGTH_SHORT).show();
                                    Common.currentuser = response.body();
                                    //TODO create password
                                    showCreatePassword();
                                }
                                else{
                                    Toast.makeText(MainActivity.this, user.getError_msg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                Toast.makeText(MainActivity.this, "User Registered Unsuccessfully"+t.getMessage(), Toast.LENGTH_SHORT).show();

                                waitingDialog.dismiss();
                            }
                        });
            }
        });

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startLoginPage();
            }
        });

        dialog.show();
    }

    private void showCreatePassword(){

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Create a Password");

        LayoutInflater inflater = this.getLayoutInflater();
        View pwdcreate_layout = inflater.inflate(R.layout.pwdcreate_layout, null);

        final MaterialEditText edt_password = pwdcreate_layout.findViewById(R.id.edt_password);
        final MaterialEditText edt_repassword = pwdcreate_layout.findViewById(R.id.edt_repassword);

        Button btn_register = pwdcreate_layout.findViewById(R.id.btn_done);

        builder.setView(pwdcreate_layout);
        final AlertDialog dialog = builder.create();

            btn_register.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    if(TextUtils.isEmpty(edt_password.getText().toString())){
                        Toast.makeText(MainActivity.this, "Please enter your Password", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(TextUtils.isEmpty(edt_repassword.getText().toString())){
                        Toast.makeText(MainActivity.this, "Please re-enter your Password", Toast.LENGTH_SHORT).show();
                        return;
                    }

                        byte[] md5Input = edt_password.getText().toString().getBytes();
                        BigInteger md5Data = null;

                        try {
                            md5Data = new BigInteger(1, md5.encryptMD5(md5Input));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        final String md5Str = md5Data.toString();

                        mService.setUserPassword(phone, md5Str).enqueue(new Callback<UserPass>() {
                            @Override
                            public void onResponse(Call<UserPass> call, Response<UserPass> response) {
                                UserPass userpass = response.body();
                                if (TextUtils.isEmpty(userpass.getError_msg())) {
                                    Toast.makeText(MainActivity.this, "Password Set Successfully", Toast.LENGTH_SHORT).show();
                                    //TODO open HomeActivity
//                                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
//                                    finish();
                                    firebaseAuth.createUserWithEmailAndPassword(phone, md5Str).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            if(!task.isSuccessful())
                                                Toast.makeText(MainActivity.this, "failed to login firbase", Toast.LENGTH_SHORT).show();
                                            else {
                                                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                                finish();
                                            }


                                        }
                                    });
                                } else {
                                    Toast.makeText(MainActivity.this, userpass.getError_msg(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<UserPass> call, Throwable t) {
                                Toast.makeText(MainActivity.this, "Unsuccessfully! Please try again" + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
            });

        dialog.setCancelable(false);

        dialog.show();
    }
}
