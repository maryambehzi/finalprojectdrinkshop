package com.maryambehzi.androiddrinkshop.Retrofit;

import com.maryambehzi.androiddrinkshop.Model.Banner;
import com.maryambehzi.androiddrinkshop.Model.Category;
import com.maryambehzi.androiddrinkshop.Model.CheckUserPassResposce;
import com.maryambehzi.androiddrinkshop.Model.CheckUserResponse;
import com.maryambehzi.androiddrinkshop.Model.Drink;
import com.maryambehzi.androiddrinkshop.Model.User;
import com.maryambehzi.androiddrinkshop.Model.UserPass;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface IDrinkShopAPI {

    @FormUrlEncoded
    @POST("checkuser.php")
    Call<CheckUserResponse> checkUserExists(@Field("phone") String phone);


    @FormUrlEncoded
    @POST("checkuserpass.php")
    Call<CheckUserPassResposce> checkUserPassExists(@Field("phone") String phone,
                                                @Field("password") String password);


    @FormUrlEncoded
    @POST("register.php")
    Call<User> registerNewUser(@Field("phone") String phone,
                               @Field("name") String name,
                               @Field("birthdate") String birthdate,
                               @Field("address") String address);


    @FormUrlEncoded
    @POST("getdrink.php")
    Observable<List<Drink>> getDrink(@Field("menuid") String menuID);


    @FormUrlEncoded
    @POST("create_password.php")
    Call<UserPass> setUserPassword(@Field("phone") String phone,
                                   @Field("password") String password);


    @FormUrlEncoded
    @POST("getuser.php")
    Call<User> getUserInformation(@Field("phone") String phone);

    @GET("getbanner.php")
    Observable<List<Banner>> getBanners();

    @GET("getmenu.php")
    Observable<List<Category>> getMenu();

    @Multipart
    @POST("upload.php")
    Call<String> uploadFile(@Part("phone") String phone, @Part MultipartBody.Part file);

    @GET("getalldrinks.php")
    Observable<List<Drink>> getAllDrinks();

    @FormUrlEncoded
    @POST("submitorder.php")
    Call<String> submitorder(@Field("price") float orderPrice,
                             @Field("orderDetail") String orderDetail,
                             @Field("comment") String comment,
                             @Field("address") String address,
                             @Field("phone") String phone);
}
