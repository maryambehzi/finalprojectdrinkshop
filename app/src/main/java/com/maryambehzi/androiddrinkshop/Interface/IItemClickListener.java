package com.maryambehzi.androiddrinkshop.Interface;

import android.view.View;

public interface IItemClickListener {
    void onClick(View v);
}
