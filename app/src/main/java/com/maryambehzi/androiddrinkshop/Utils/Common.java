package com.maryambehzi.androiddrinkshop.Utils;

import com.maryambehzi.androiddrinkshop.Database.DataSource.CartRepository;
import com.maryambehzi.androiddrinkshop.Database.DataSource.FavoriteRepository;
import com.maryambehzi.androiddrinkshop.Database.Local.DrinkShopRoomDatabase;
import com.maryambehzi.androiddrinkshop.Model.Category;
import com.maryambehzi.androiddrinkshop.Model.Drink;
import com.maryambehzi.androiddrinkshop.Model.User;
import com.maryambehzi.androiddrinkshop.Retrofit.IDrinkShopAPI;
import com.maryambehzi.androiddrinkshop.Retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

public class Common {

    //In Emulator, localhost = 10.0.2.2
    //TODO move all these shits to a real server
//    private static final String BASE_URL = "http://192.168.1.164/Project/";
//    public static final String BASE_URL = "http://192.168.0.220/Project/";
        public static final String BASE_URL = "http://mary.nazarbazi.ir/";

    private static final String TOPPING_MENU_ID = "7";
    public static User currentuser = null;
    public static Category currentCategury = null;
    public static List<Drink> toppinglist = new ArrayList<>();

    //Database
    public static DrinkShopRoomDatabase cartDatabase;
    public static CartRepository cartRepository;
    public static FavoriteRepository favoriteRepository;

    //Hold field
    public static int size_of_cup = -1;  //nothing is choosen 0:M 1:L
    public static int sugar = -1;
    public static int ice = -1;
    public static IDrinkShopAPI getAPI(){
        return RetrofitClient.getClient(BASE_URL).create(IDrinkShopAPI.class);
    }

}
