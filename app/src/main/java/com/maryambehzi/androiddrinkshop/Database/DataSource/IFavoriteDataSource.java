package com.maryambehzi.androiddrinkshop.Database.DataSource;

import androidx.room.Delete;
import androidx.room.Query;

import com.maryambehzi.androiddrinkshop.Database.ModelDB.Favorite;

import java.util.List;

import io.reactivex.Flowable;

public interface IFavoriteDataSource {
    Flowable<List<Favorite>> getFavItems();

    int isFavorite(int itemId);

    void insertFave(Favorite...favorites);


    void delete(Favorite favorite);
}
