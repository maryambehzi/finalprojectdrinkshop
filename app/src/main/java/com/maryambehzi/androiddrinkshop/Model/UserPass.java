package com.maryambehzi.androiddrinkshop.Model;

public class UserPass {

    private String phone;
    private String password;

    private String error_msg; //It will empty if user return object

    public UserPass(){

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

}
