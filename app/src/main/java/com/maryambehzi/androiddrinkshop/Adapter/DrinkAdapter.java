package com.maryambehzi.androiddrinkshop.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.gson.Gson;
import com.maryambehzi.androiddrinkshop.Database.ModelDB.Cart;
import com.maryambehzi.androiddrinkshop.Database.ModelDB.Favorite;
import com.maryambehzi.androiddrinkshop.Interface.IItemClickListener;
import com.maryambehzi.androiddrinkshop.Model.Drink;
import com.maryambehzi.androiddrinkshop.R;
import com.maryambehzi.androiddrinkshop.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DrinkAdapter extends RecyclerView.Adapter<DrinkViewHolder> {

    Context context;
    List<Drink> drinkList;

    public DrinkAdapter(Context context, List<Drink> drinkList) {
        this.context = context;
        this.drinkList = drinkList;
    }

    @NonNull
    @Override
    public DrinkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.drink_item_layout, null);
        return new DrinkViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final DrinkViewHolder holder, final int position) {

        holder.txt_price.setText(new StringBuilder("$").append((drinkList.get(position).Price).toString()));
        holder.txt_drink_name.setText(drinkList.get(position).Name);

        holder.btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddToCartDialog(position);
            }
        });

        Picasso.with(context)
                .load(drinkList.get(position).Link)
                .into(holder.img_product);

        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        //Favorite
        if (Common.favoriteRepository.isFavorite(Integer.parseInt(drinkList.get(position).ID)) == 1 )
            holder.btn_favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
        else
            holder.btn_favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);

        holder.btn_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.favoriteRepository.isFavorite(Integer.parseInt(drinkList.get(position).ID)) != 1 )
                {
                    addOrRemoveFavorite(drinkList.get(position),true);
                    holder.btn_favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                }
                else
                {
                    addOrRemoveFavorite(drinkList.get(position), false);
                    holder.btn_favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                }
            }
        });
    }

    private void addOrRemoveFavorite(Drink drink, boolean isAdd) {
        Favorite favorite = new Favorite();
        favorite.id = drink.ID;
        favorite.link = drink.Link;
        favorite.name = drink.Name;
        favorite.price = drink.Price;
        favorite.menuId = drink.MenuId;

        if (isAdd)
            Common.favoriteRepository.insertFave(favorite);
        else
            Common.favoriteRepository.delete(favorite);
    }

    private void showAddToCartDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.add_to_cart_layout, null);

        //View
        ImageView img_product_dialog = (ImageView) itemView.findViewById(R.id.img_cart_product);
        final ElegantNumberButton txt_count = (ElegantNumberButton) itemView.findViewById(R.id.txt_count);
        TextView txt_product_dialog = (TextView) itemView.findViewById(R.id.txt_cart_product_name);

        EditText edt_comment = (EditText) itemView.findViewById(R.id.edt_comment);

        RadioButton rdi_sizem = (RadioButton) itemView.findViewById(R.id.rdi_sizem);
        RadioButton rdi_sizel = (RadioButton) itemView.findViewById(R.id.rdi_sizel);

        rdi_sizem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.size_of_cup = 0;
            }
        });
        rdi_sizel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.size_of_cup = 1;
            }
        });

        RadioButton rdi_suagr100 = (RadioButton) itemView.findViewById(R.id.rdi_sugar100);
        RadioButton rdi_suagr70 = (RadioButton) itemView.findViewById(R.id.rdi_sugar70);
        RadioButton rdi_suagr50 = (RadioButton) itemView.findViewById(R.id.rdi_sugar50);
        RadioButton rdi_suagr30 = (RadioButton) itemView.findViewById(R.id.rdi_sugar30);
        RadioButton rdi_suagrfree = (RadioButton) itemView.findViewById(R.id.rdi_sugarfree);

        rdi_suagr100.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.sugar = 100;
            }
        });
        rdi_suagr70.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.sugar = 70;
            }
        });
        rdi_suagr50.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.sugar = 50;
            }
        });
        rdi_suagr30.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.sugar = 30;
            }
        });
        rdi_suagrfree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.sugar = 0;
            }
        });

        RadioButton rdi_ice100 = (RadioButton) itemView.findViewById(R.id.rdi_ice100);
        RadioButton rdi_ice70 = (RadioButton) itemView.findViewById(R.id.rdi_ice70);
        RadioButton rdi_ice50 = (RadioButton) itemView.findViewById(R.id.rdi_ice50);
        RadioButton rdi_ice30 = (RadioButton) itemView.findViewById(R.id.rdi_ice30);
        RadioButton rdi_icefree = (RadioButton) itemView.findViewById(R.id.rdi_icefree);

        rdi_ice100.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.ice = 100;
            }
        });
        rdi_ice70.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.ice = 70;
            }
        });
        rdi_ice50.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.ice = 50;
            }
        });
        rdi_ice30.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.ice = 30;
            }
        });
        rdi_icefree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    Common.ice = 0;
            }
        });

        //set data
        Picasso.with(context)
                .load(drinkList.get(position).Link)
                .into(img_product_dialog);
        txt_product_dialog.setText(drinkList.get(position).Name);

        builder.setView(itemView);
        builder.setNegativeButton("ADD TO CART", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(Common.size_of_cup == -1 || Common.sugar == -1 ||Common.ice == -1){
                    Toast.makeText(context, "Please fill all required fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                showConfirmDialog(position, txt_count.getNumber());
                dialog.dismiss();
            }
        });



        builder.show();



    }

    private void showConfirmDialog(final int position, final String number) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.confirm_add_to_cart_layout, null);

        //View
        ImageView img_product_dialog = (ImageView) itemView.findViewById(R.id.img_product);
        final TextView txt_product_dialog = (TextView) itemView.findViewById(R.id.txt_cart_product_name);
        TextView txt_product_price = (TextView) itemView.findViewById(R.id.txt_cart_product_price);
        TextView txt_suagr = (TextView) itemView.findViewById(R.id.txt_sugar);
        TextView txt_ice = (TextView) itemView.findViewById(R.id.txt_ice);

        //set data
        Picasso.with(context).load(drinkList.get(position).Link).into(img_product_dialog);
        txt_product_dialog.setText(new StringBuilder(drinkList.get(position).Name).append("  x")
                .append(number)
                .append(Common.size_of_cup == 0 ? " Size M":" Size L").toString());

        txt_ice.setText(new StringBuilder("Ice: ").append(Common.ice).append("%").toString());
        txt_suagr.setText(new StringBuilder("Sugar: ").append(Common.sugar).append("%").toString());

        double price = (Double.parseDouble(drinkList.get(position).Price) * Double.parseDouble(number));

        if(Common.size_of_cup == 1) //size L
            price += 2.0;

        txt_product_price.setText(new StringBuilder("$").append(price));

        final double finalPrice = price;
        builder.setNegativeButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                try {


                    //Add to SQL
                    //Create new cart item
                    Cart cartItem = new Cart();
                    cartItem.name = txt_product_dialog.getText().toString();
                    cartItem.amount = Integer.parseInt(number);
                    cartItem.ice = Common.ice;
                    cartItem.sugar = Common.sugar;
                    cartItem.price = finalPrice;
                    cartItem.link = drinkList.get(position).Link;

                    //Add to DB
                    Common.cartRepository.insertToCart(cartItem);

                    Log.d("DrinkShop Debug", new Gson().toJson(cartItem));

                    Toast.makeText(context, "added to cart successfuly", Toast.LENGTH_SHORT).show();
                }
                catch (Exception ex){
                    Toast.makeText(context, "failed adding to cart /n"+ex.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        });

        builder.setView(itemView);
        builder.show();


    }

    @Override
    public int getItemCount() {
        return drinkList.size();
    }
}
