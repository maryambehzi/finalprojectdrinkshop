package com.maryambehzi.androiddrinkshop.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.maryambehzi.androiddrinkshop.DrinkActivity;
import com.maryambehzi.androiddrinkshop.Interface.IItemClickListener;
import com.maryambehzi.androiddrinkshop.Model.Category;
import com.maryambehzi.androiddrinkshop.R;
import com.maryambehzi.androiddrinkshop.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    Context mcontext;
    List<Category> categories;

    public CategoryAdapter(Context context, List<Category> categories){
        this.mcontext = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemview = LayoutInflater.from(context).inflate(R.layout.menu_item_layout, null);
        return new CategoryViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {

        final Context scontext = holder.img_product.getContext();

        //Load Image
        Picasso.with(scontext)
                .load(categories.get(position).Link)
                .into(holder.img_product);

        holder.txt_menu_name.setText(categories.get(position).Name);


        //Event
        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onClick(View v) {

                Common.currentCategury = categories.get(position);

                //start new Activity
                scontext.startActivity(new Intent(scontext, DrinkActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
