package com.maryambehzi.androiddrinkshop.ui.slideshow;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.maryambehzi.androiddrinkshop.Adapter.FavoriteAdapter;
import com.maryambehzi.androiddrinkshop.Database.ModelDB.Favorite;
import com.maryambehzi.androiddrinkshop.R;
import com.maryambehzi.androiddrinkshop.Utils.Common;
import com.maryambehzi.androiddrinkshop.Utils.RecyclerItemTouchHelper;
import com.maryambehzi.androiddrinkshop.Utils.RecyclerItemTouchHelperListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SlideshowFragment extends Fragment implements RecyclerItemTouchHelperListener {

    RecyclerView recycler_fav;

    RelativeLayout rootLayout;
    CompositeDisposable compositeDisposable;
    FavoriteAdapter favoriteAdapter;
    List<Favorite> localFavorites = new ArrayList<>();
    RecyclerItemTouchHelperListener listener;

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);

        compositeDisposable = new CompositeDisposable();

//        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);

        recycler_fav = (RecyclerView) root.findViewById(R.id.recycler_fav);
        recycler_fav.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_fav.setHasFixedSize(true);

        ItemTouchHelper.SimpleCallback simpleCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recycler_fav);

        loadFavoritesItem();
        return root;
    }

    private void loadFavoritesItem() {

        compositeDisposable.add(Common.favoriteRepository.getFavItems()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Favorite>>() {
                    @Override
                    public void accept(List<Favorite> favorites) throws Exception {

                        displayFavoriteItem(favorites);
                    }
                }));
    }

    private void displayFavoriteItem(List<Favorite> favorites) {

        localFavorites = favorites;

        favoriteAdapter = new FavoriteAdapter(getContext(), favorites);
        recycler_fav.setAdapter(favoriteAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        loadFavoritesItem();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int detection, int position) {

        if (viewHolder instanceof FavoriteAdapter.FavoriteViewHolder)
        {
            String name = localFavorites.get(viewHolder.getAdapterPosition()).name;

            final Favorite deleteItem = localFavorites.get(viewHolder.getAdapterPosition());
            final int deleteIndex = viewHolder.getAdapterPosition();

            //Delete Item from adapter
            favoriteAdapter.removeItem(deleteIndex);
            // Delete Item from Room database
            Common.favoriteRepository.delete(deleteItem);

            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), new StringBuilder(name)
                    .append(" removed from Favorites List  ")
                    .toString(), Snackbar.LENGTH_LONG);

            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    favoriteAdapter.restoreItem(deleteItem, deleteIndex);
                    Common.favoriteRepository.insertFave(deleteItem);
                }
            });

            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}