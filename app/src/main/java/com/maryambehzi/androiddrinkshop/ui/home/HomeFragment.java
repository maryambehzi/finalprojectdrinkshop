package com.maryambehzi.androiddrinkshop.ui.home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.maryambehzi.androiddrinkshop.Adapter.CategoryAdapter;
import com.maryambehzi.androiddrinkshop.Database.DataSource.CartRepository;
import com.maryambehzi.androiddrinkshop.Database.DataSource.FavoriteRepository;
import com.maryambehzi.androiddrinkshop.Database.DataSource.ICartDataSource;
import com.maryambehzi.androiddrinkshop.Database.Local.CartDataSource;
import com.maryambehzi.androiddrinkshop.Database.Local.DrinkShopRoomDatabase;
import com.maryambehzi.androiddrinkshop.Database.Local.FavoriteDataSource;
import com.maryambehzi.androiddrinkshop.HomeActivity;
import com.maryambehzi.androiddrinkshop.MainActivity;
import com.maryambehzi.androiddrinkshop.Model.Banner;
import com.maryambehzi.androiddrinkshop.Model.Category;
import com.maryambehzi.androiddrinkshop.R;
import com.maryambehzi.androiddrinkshop.Retrofit.IDrinkShopAPI;
import com.maryambehzi.androiddrinkshop.Utils.Common;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static androidx.recyclerview.widget.LinearLayoutManager.*;

public class HomeFragment extends Fragment {
    IDrinkShopAPI mService;

    RecyclerView lst_menu;
    Context context;

    ImageView cart_icon;

    CircleImageView img_avatar;

    Uri selectFileUrl;
    
    //Rxjava
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        View inflatedView = getLayoutInflater().inflate(R.layout.fragment_home, null);
        final TextView textView = root.findViewById(R.id.text_home);
        final SliderLayout sliderLayout = (SliderLayout) inflatedView.findViewById(R.id.slider);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        mService = Common.getAPI();

        lst_menu = (RecyclerView) inflatedView.findViewById(R.id.lst_menu);
        lst_menu.setLayoutManager(new LinearLayoutManager(context, HORIZONTAL,false));
        lst_menu.setHasFixedSize(true);

        compositeDisposable.add(mService.getBanners()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Banner>>() {
                    @Override
                    public void accept(List<Banner> banners) throws Exception {
                        HashMap<String,String> bannerMap = new HashMap<>();
                        for(Banner item:banners)
                            bannerMap.put(item.getName(),item.getLink());
                            //bannerMap.put("ali","http://www.digicast.cn/images/pics/2017083103430111372.jpg");
                            //bannerMap.put("mamad","http://www.digicast.cn/images/pics/2017083103430111372.jpg");


                        for(String name:bannerMap.keySet()){
                            TextSliderView textSliderView = new TextSliderView(getContext());
                            textSliderView.description(name)
                                    .image(bannerMap.get(name))
                                    .setScaleType(BaseSliderView.ScaleType.Fit);
                            sliderLayout.addSlider(textSliderView);
                        }
                    }
                }));

        compositeDisposable.add(mService.getMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Category>>() {
                    @Override
                    public void accept(List<Category> categories) throws Exception {
                        CategoryAdapter adapter = new CategoryAdapter(context, categories);
                        lst_menu.setAdapter(adapter);
                    }}));


        Context mcontex = container.getContext();
        //init DB
        Common.cartDatabase = DrinkShopRoomDatabase.getInstance(mcontex);
        Common.cartRepository = CartRepository.getInstance(CartDataSource.getInstance(Common.cartDatabase.cartDAO()));
        Common.favoriteRepository = FavoriteRepository.getInstance(FavoriteDataSource.getInstance(Common.cartDatabase.favoriteDAO()));



        return inflatedView;
    }
}